.. include:: ../Includes.txt

.. _changelog:

==========
Change log
==========

Version 11.0.3
-------------

- small fixes on ext_emconf.php
- small fixes on composer.json


Version 11.0.2
-------------

- migrate to typo3 v11 and refactor
- bugfix in migrategeneral-Action
- migrate backend templates and css to bootstrap 5
- include bugfix "Columns not migrate, when based on container with empty column."
- version to only typo3 v11

Thanks to the contributers: @stigfaerch and @florian.obwegs
